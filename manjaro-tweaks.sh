#!/bin/sh

# Remove unwanted Desktop files
rm -v /usr/share/applications/avahi-discover.desktop
rm -v /usr/share/applications/bssh.desktop
rm -v /usr/share/applications/bvnc.desktop
rm -v /usr/share/applications/qv4l2.desktop
rm -v /usr/share/applications/qvidcap.desktop
rm -v /usr/share/applications/UserFeedbackConsole.desktop
